import React, { Component } from 'react';
import { Provider } from 'react-redux';
import ImageViewer from 'components/image-viewer';
import store from 'stores';

class Plugin extends Component {
  static displayName = 'ImageViewerPlugin';

  /**
   * Connect the Plugin to the store and render.
   *
   * @returns {React.Component} The rendered component.
   */
  render() {
    return (
      <Provider store={store}>
        <ImageViewer />
      </Provider>
    );
  }
}

export default Plugin;
