const PREFIX = 'imageViewer';

export const QUERY_APPLIED = `${PREFIX}/QUERY_APPLIED`;

export const INITIAL = {};

const reducer = (state = INITIAL, action) => {
  console.log("dispatching:", action.type, action.value)
  switch (action.type) {
    case QUERY_APPLIED:
      return ({...state, ...action.value})
    default:
      return state;
  }
};

export default reducer;

export const queryApplied = (value) => ({
  type: QUERY_APPLIED,
  value
});
