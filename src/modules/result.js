const PREFIX = 'imageViewer';

export const RESET_RESULT = `${PREFIX}/RESET_RESULT`;
export const INSERT_RESULT = `${PREFIX}/INSERT_RESULT`;
export const RESULT_IMAGES = `${PREFIX}/RESULT_IMAGES`;

export const INITIAL = new Map()

const reducer = (state = INITIAL, action) => {
  console.log("dispatching:", action.type)
  switch (action.type) {
    case RESET_RESULT:
      return (INITIAL)
    case INSERT_RESULT:
      console.log("Insert result:", action.id, action.values)
      const id = action.id
      return({...state, [id] : (state[id] || []).concat(action.values)})
    default:
      return state;
  }
};

export default reducer;

export const resetResult = () => ({
  type: RESET_RESULT,
});

export const insertResult = (id, values) => ({
  type: INSERT_RESULT, id, values
});
