/**
 * The module action prefix.
 */
const PREFIX = 'imageViewer';

/**
 * The status action type.
 */
export const SET_STATUS = `${PREFIX}/SET_STATUS`;
export const REQUEST_IMAGES = `${PREFIX}/REQUEST_IMAGES`;

/**
 * The initial state.
 */
export const INITIAL_STATE = 'initial';

/**
 * Reducer function for handle state changes to status.
 *
 * @param {String} state - The status state.
 * @param {Object} action - The action.
 *
 * @returns {String} The new state.
 */
const reducer = (state = INITIAL_STATE, action) => {
  console.log("dispatch:", action.type)
  switch (action.type) {
    case SET_STATUS:
      if (action.value == null)
        return (state === 'initial') ? 'done' : 'initial';
      return (action.value)
    case 'call':
      if (typeof action.value === 'function') {
        console.log("Return a thunk")
        return ((dsp, getState) => action.value(dsp, getState));
      } else {
        console.log("Not a thunk")
        break;
      }
    default:
      return state;
    }
};

export default reducer;

/**
 * Action creator for toggle status events.
 *
 * @returns {Object} The toggle status action.
 */
export const setStatus = (value) => ({
  type: SET_STATUS,
  value
});
