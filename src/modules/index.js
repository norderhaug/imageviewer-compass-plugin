import { combineReducers } from 'redux';
import status from './status';
import result from './result';
import query from './query';
import dataService from './data-service'


const reducer = combineReducers({
  status,
  query,
  result,
  dataService
});

export default reducer;
