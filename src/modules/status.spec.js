import reducer, { setStatus, TOGGLE_STATUS } from 'modules/status';

describe('status module', () => {
  /*
  describe('#reducer', () => {
    context('when the action type is unknown', () => {
      it('returns the state', () => {
        expect(reducer('disabled', {})).to.equal('disabled');
      });
    });

    context('when the action type is TOGGLE_STATUS', () => {
      it('returns the toggled state', () => {
        expect(reducer('disabled', setStatus())).to.equal('enabled');
      });
    });
  });
  */
  describe('#setStatus', () => {
    it('returns the action', () => {
      expect(setStatus()).to.deep.equal({
        type: TOGGLE_STATUS
      });
    });
  });
});
