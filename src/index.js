import ImageViewerPlugin from './plugin';
import ImageViewerStore from 'stores';

/**
 * A sample role for the component.
 */
const ROLE = {
  name: 'ImageViewer',
  component: ImageViewerPlugin
};

/**
 * Activate all the components in the Image Viewer package.
 * @param {Object} appRegistry - The Hadron appRegisrty to activate this plugin with.
 **/
function activate(appRegistry) {
  // Register the ImageViewerPlugin as a role in Compass
  //
  // Available roles are:
  //   - Instance.Tab: { name: <String>, component: <React.Component>, order: <Number> }
  //   - Database.Tab: { name: <String>, component: <React.Component>, order: <Number> }
  //   - Collection.Tab: { name: <String>, component: <React.Component>, order: <Number> }
  //   - CollectionHUD.Item: { name <String>, component: <React.Component> }
  //   - Header.Item: { name: <String>, component: <React.Component>, alignment: <String> }

  appRegistry.registerRole('Collection.Tab', ROLE);
  appRegistry.registerStore('ImageViewer.Store', ImageViewerStore);
}

/**
 * Deactivate all the components in the Image Viewer package.
 * @param {Object} appRegistry - The Hadron appRegisrty to deactivate this plugin with.
 **/
function deactivate(appRegistry) {
  appRegistry.deregisterRole('Collection.Tab', ROLE);
  appRegistry.deregisterStore('ImageViewer.Store');
}

export default ImageViewerPlugin;
export { activate, deactivate };
