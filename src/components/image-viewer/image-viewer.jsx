import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import ToggleButton from 'components/toggle-button';
import { setStatus } from 'modules/status';
import { requestImages } from 'modules/request';
import { insertResult, resetResult } from 'modules/result';
import Preview from 'components/preview';

import styles from './image-viewer.less';

class ImageViewer extends Component {
  static displayName = 'ImageViewerComponent';

  static propTypes = {
    result: PropTypes.object.isRequired,
    status: PropTypes.oneOf(['initial', 'fetching', 'done', 'outdated']),
    error: PropTypes.object,
    setStatus: PropTypes.func.isRequired,
  };

  static defaultProps = {
    result: new Map(),
    error: null,
    status: 'initial'
  };

  onClick = () => {
    this.props.setStatus()
  }

  onApplyClicked() {
      console.log("Apply Clicked")
      this.props.requestImages(this.props)
  }

  onResetClicked() {
      console.log("Reset Clicked")
      this.props.resetResult()
      this.props.setStatus('initial')
  }

  componentWillMount() {
    this.queryBar = window.app.appRegistry.getComponent('Query.QueryBar');
  }

  renderError = () =>
      (this.props.error)
      ? <div>{this.props.error}</div>
      : null;

  renderContent() {
      const result = this.props.result
      return (
        <div className="column-container">
          {this.renderError()}
          <div className="column main">
             { Object.entries(result)
                     .map(([id, group]) =>
                           <Preview key={id} doc={id} group={group} />)
             }
          </div>
        </div>
      )
    }

  /**
   * Render ImageViewer component.
   *
   * @returns {React.Component} The rendered component.
   */
  render() {
    return (
      <div className={classnames(styles.root)}>
        {/* <p>The current status is: <code>{this.props.status}</code></p> */}
        {/* <ToggleButton onClick={this.onClick} /> */}
        <div className="controls-container">
          <this.queryBar
            buttonLabel="Find"
            onApply={this.onApplyClicked.bind(this)}
            onReset={this.onResetClicked.bind(this)} />
        </div>
        {this.renderContent()}
      </div>
    );
  }
}

/**
 * Map the store state to properties to pass to the components.
 *
 * @param {Object} state - The store state.
 *
 * @returns {Object} The mapped properties.
 */
const mapStateToProps = (state) => ({
  result: state.result,
  storeState: state.storeState, // not used
  error: state.error,
  status: state.status
});

const mapDispatchToProps = {
  setStatus,
  requestImages,
  insertResult,
  resetResult,
  dispatch: x => x  // development convenience
}

/**
 * Connect the redux store to the component.
 * (dispatch)
 */
const MappedImageViewer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ImageViewer);

export default MappedImageViewer;
export { ImageViewer };
