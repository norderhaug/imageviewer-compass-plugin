import React from 'react';
import { mount } from 'enzyme';

import { ImageViewer } from 'components/image-viewer';
import ToggleButton from 'components/toggle-button';
import styles from './image-viewer.less';

describe('ImageViewer [Component]', () => {
  let component;
  let setStatus;

  beforeEach(() => {
    setStatus = sinon.spy();
    component = mount(<ImageViewer setStatus={setStatus} status="initial" />);
  });

  afterEach(() => {
    component = null;
    setStatus = null;
  });

  it('renders the correct root classname', () => {
    expect(component.find(`.${styles.root}`)).to.be.present();
  });

  it('should contain one <h2> tag', () => {
    expect(component.find('h2')).to.have.length(1);
  });

  it('should contain one <ToggleButton />', () => {
    expect(component.find(ToggleButton)).to.have.length(1);
  });

  it('should initially have prop {status: \'initial\'}', () => {
    expect(component.prop('status')).to.equal('initial');
  });
});
