import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import styles from './preview.less';

class Preview extends Component {
  static displayName = 'Preview';

  static propTypes = {
    doc: PropTypes.object,
    group: PropTypes.array.isRequired,
    children: PropTypes.node
  };

  static defaultProps = {
    doc: {},
    group: [],
    children: 'preview'
  };

/*
  componentDidMount() {
    this.noop();
  }

  // A no-operation so that the linter passes for the compass-plugin template,
  // without the need to an ignore rule, because we want the linter to fail when this
  // dependency is "for-real" not being used (ie: in an actual plugin).
  noop = () => {
    const node = ReactDOM.findDOMNode(this);
    return node;
  };
  */

  renderMatch(match) {
    console.log ("Render Match:", match)
    return (
      <div className={classnames(styles.document)}>
        <div className={classnames(styles['document-element'])}>
          <img className={classnames(styles['preview-image'])}
                          controls src={match.href} />
        </div>
      </div>)
  }

  render () {
      console.log ("Render Preview:", this.props)
      return(
        <div>
          <div className={classnames(styles['preview-toolbar'])}>
            <div className={classnames(styles['preview-toolbar-text'])}>
                      <code>id: {this.props.doc}</code>
            </div>
          </div>
          <div className={classnames(styles.preview)}>
            <div className={classnames(styles['preview-documents'])}>
              {this.props.group
               .filter((match) => match && match.href)
               .map( this.renderMatch )}
            </div>
          </div>
        </div>)
      }

}

export default Preview;
export { Preview };
